Hôte SSH : ssh-birdzoo.alwaysdata.net (accessible également par le Web)
Adresse AlwaysData : birdzoo.alwaysdata.net

Prise en charge : Pas de mauvaise connexion
			Pas d'inscription mauvaise user
			Confirmation de mot de passe a l'inscription
			Pas de ticket vide : Priorité et Statut obligatoire -> On peut l'étendre aux autres cases de form Ticket
			Hachage du mot de passe		


											Connexion:

Administrateur:

Username: admin
Password: 1234



	User:

Username: testUser
Password: 1234

									   Fichier de pages->Fichier contenant des fonctions

index.php

profile.php
	
connexion.php->authent.php		
connexion.php->inscrip.php		// Possibilité de s'inscrire dans cette même page

map.php

discover.php

inscription.php->inscrip.php

formTicket.php->recupTicket.php

afficherTicket.php

afficherTicket.php->resolveTicket.php //Possibilité de résoudre depuis ce même fichier

											GRILLE EVALUATION

- gitlab OK
- charte graphique au format PDF et à réutiliser dans les pages OK

- creer des pages HTML + CSS (au moins la page d'accueil + connexion/inscription/ajout tickets/visualisation des tickets par l'administrateur) avec des liens vers le reste du site (pages WordPress)							 OK

- formulaire tickets OK

- Page index modifiée et maintenant dymanique en fonction de l'heure OK

- inscription	OK
- connexion	OK

- sessions PHP (rester connecté)	OK

- creation ticket	OK

- affichage liste de tous les tickets (admin connecté)	OK

- affichage liste de tous les tickets émis (utilisateur connecté)

- affichage les détails d'1 seul ticket (admin connecté) + modifications / résolution possible OK

- mots de passe en BDD chiffrés correctement avec des aléas OK

- BDD (fournir le schéma complet des tables de la BDD (pas celles de WORDPRESS !!!) avec leurs relations) OK

- utiliser requetes asynchrones (instruction JS fetch) pour modifier les pages sans les recharger (ex1: mettre a jour la liste des tickets automatiquement quand on est sur la page qui les affiche tous / ou ex2 : changer les images de la page d'accueil toutes les 5 s) 

- déployer sur un serveur distant (alwaysdata) et stockez un fichier du type README dans votre gitlab en indiquant l'adresse de votre site distant pour que je puisse le retrouver facilement!) 

BUG BDD - > DEPLOIMENT OK

- Realiser le diagramme des cas d'utilisation de votre site internet (use case diagram) OK

										
										