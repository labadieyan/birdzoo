<?php


session_start();
    $login="";
    if(isset($_SESSION["name"])){
        $login=$_SESSION["name"];
    }

    // MSG and MSG2 GET FROM authent.php IN ORDER TO SHOW A MESSAGE IN THIS PAGE 
    // DEPENDS FROM THE RESULTS of authent.php

    $msg="";
    if(isset($_GET["msg"])){
        $msg=$_GET["msg"];
    }

    $msg2="";
    if(isset($_GET["msg"])){
        $msg=$_GET["msg"];
    }

?>

<code><!DOCTYPE html>
    <html>
        <head>
        <meta charset="utf-8">
        <title>BirdZoo</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
        <!-- Bootstrap -->
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    
        <!-- Font Awesome -->
        <script src="https://kit.fontawesome.com/541db90648.js" crossorigin="anonymous"></script>
    
        <link rel="icon" type="image/png" href="R.jfif">

        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="form.css" >

        </head>
    
        <body class="bg-dark">
            <section>
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top p-0 pl-1">
                <a class="navbar-brand" > 

                </a>
            
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item" id="navigation-accueil">
                            <a class="nav-link" href="index.php">Homepage</a>
                        </li>
                        <li class="nav-item" id="navigation-evenement">
                            <a class="nav-link" href="discover.php">Discover</a>
                        </li>
                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="map.php">Préparer sa visite</a>
                        </li>
                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="formTicket.php">Réclamation</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <?php 
                                if($login!=""){
                                    echo("<li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"profile.php\">$login</a> </li>
                                        <li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"./disconnect.php\">Déconnexion</a> </li>");
                                }
                                else{
                                    echo("<a class=\"nav-link\" href=\"connexion.php\">Connexion</a>");
                                }
                            ?>
                        </li>
                        
                    </ul>
                </div>
            </section>
        
            <section class="element mt-5">
                
                <!-- CONNECTION FORM -->
                <article class="shadow-lg bg-dark rounded p-5 mb-5">
                    <form class="form bg-dark" action="./authent.php" method="post">
                        <center>
                            <h2><i>Connexion</i></h2>
                            <p><input name="login" class="col-sm-12 form-control mt-5" type="text" placeholder="Nom d'utilisateur"></input></p>
                            <p><input name="pass" class="col-sm-12 form-control mt-3" type="password" placeholder="Mot de passe"></input></p>
                            <?php
                                echo("<p class=\"error mt-3\">$msg</p>");
                                echo("<p class=\"error mt-3\">$msg2</p>");
                            ?>
                            <input class="btn btn-secondary mt-3" type="submit" value="Se connecter"/>
                        </center>
                    </form>        
                </article>
            
                <!-- INSCRIPTION FORM -->
                <article class="shadow-lg bg-dark rounded p-5">
                    <form class="form bg-dark" action="./inscrip.php" method="post">
                        <center><h2><i>Inscription</i></h2>
                        <p> Veuillez entrez vos informations personnelles afin de vous inscrire : </p>
                        <p><input name="login" class="col-sm-12 form-control mt-5" type="text" placeholder="Nom d'utilisateur"></input></p>
                        <p><input name="pass" class="col-sm-12 form-control mt-3" type="password" placeholder="Mot de passe"></input></p>
                        <p><input name="cpass" class="col-sm-12 form-control mt-3" type="password" placeholder="Confirmez mot de passe"></input></p>
                        <p><input name="date" class="col-sm-12 form-control mt-3" type="date" ></p>
                        <input class="btn btn-secondary mt-3" type="submit" value="Valider"/></center>
                    </form>
                    
                </article>
            </section>

            
            <footer class="pied-de-page bg-dark mt-auto d-flex flex-column">
                <section class="d-flex flex-row justify-content-around pt-3 ">
                        
                    <ul class="text-center">
                        <li><a>Politique de confidentialité</a></li>
                        <li><a>Conditions Générales d'utilisation</a></li>
                        <li><a>Informations Légales</a></li>
                        <li><a>F.A.Q</a></li>
                    </ul>
                                
                    <ul>
                        <li>
                            <a><i class="fa fa-instagram" aria-hidden="true"></i> Suivez nous sur Instagram </a>
                        </li>
                        <li>
                            <a><i class="fa fa-twitter" aria-hidden="true"></i> Suivez nous sur Twitter </a>                            
                        </li>
                    </ul>
                </section>
                
                <p class="colored text-center">
                    Copyright &copy; 2022 BirdZoo. All right reserved.
                </p>
            </footer>

        </body>
    </html>
</code>


