<?php


session_start();
$login="";
if(isset($_SESSION["name"])){
    $login=$_SESSION["name"];
}

$msg="";
if(isset($_GET["msg"])){
    $msg=$_GET["msg"];
}

$servername = "mysql-birdzoo.alwaysdata.net";
$dbname = "birdzoo_bdd";
$username = "birdzoo";
$mdp ="6z9r2uYT";

try{
    $pdo = new PDO("mysql:host=$servername;dbname=$dbname", "$username", "$mdp");
}

catch(PDOException $pdoe){
    echo("Can't access to the database");
    exit();
}

function selectAllAndDisplay($pdo){

    // Select all tickets from the DB 'reports'

    $sql_query = "SELECT * from `reports`;";
    $statement = $pdo->query($sql_query);
    if( $statement === FALSE ){
        echo("Bad request ($sql_query)");
        exit();
    }

    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    if($results){
        foreach ($results as $entry) {
            //print the results
            echo("<tr>");
            echo("<th>".$entry['id']."</th>"."<th>".$entry['name']."</th>"."<th>".$entry['surname']."</th>"."<th>".$entry['mail']."</th>"."<th>".$entry['dsc']."</th>"."<th>".$entry['prio']."</th>"."<th>".$entry['location']."</th>"."<th>".$entry['statut']."</th>"."<th>".$entry['time']."</th>");
            echo("</tr>");
        }
        
    }
}


?>


<code><!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>BirdZoo</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
            <!-- Bootstrap -->
            <!-- jQuery library -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <!-- Popper JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <!-- Latest compiled JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        
            <!-- Font Awesome -->
            <script src="https://kit.fontawesome.com/541db90648.js" crossorigin="anonymous"></script>

            <link rel="stylesheet" href="style.css">
        </head>

        <body class="bg-dark">
            <section>
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top p-0 pl-1">
                <a class="navbar-brand" > 

                </a>
            
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item" id="navigation-accueil">
                            <a class="nav-link" href="index.php">Homepage</a>
                        </li>
                        <li class="nav-item" id="navigation-evenement">
                            <a class="nav-link" href="zoo.html">Découvrir le Zoo</a>
                        </li>
                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="visite.html">Préparer sa visite</a>
                        </li>
                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="formTicket.php">Réclamation</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <?php 
                                if($login!=""){
                                    echo("<li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"profile.php\">$login</a> </li>
                                        <li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"./disconnect.php\">Déconnexion</a> </li>");
                                }
                                else{
                                    echo("<a class=\"nav-link\" href=\"connexion.php\">Connexion</a>");
                                }
                            ?>
                        </li>

                    </ul>
                </div>
            </section>


            <section class="element mt-5">
                <!-- 2 SEARCH BAR FOR RESEARCHING A TICKET OR SOLVING BY ID ENTERED -->
                <article class=" shadow-lg bg-dark rounded p-5 d-flex flex-column justify-content-around">
                    <center>
                        <h2 style="text-align:center">Searching a Ticket ?</h2><br>
                        If you search a Ticket, please fill the form below<br>
                        <div class=form>
                            <form id="form" method="GET" action="./searchTicket.php">
                                <input name="id" placeholder="ID">
                                <input type="submit" value="Search"/>
                            </form>
                        </div>

                        If a Ticket is solved, please fill the form below<br>
                        <div class=form>
                            <form id="form" method="POST" action="./resolveTicket.php">
                                <input name="id" id="id" placeholder="ID" >
                                <input type="submit" value="Search">
                            </form>
                        </div>
                        
                        <?php 
                            if($msg!=""){
                                echo("<p class=\"colored\" > Ticket is <strong> $msg </strong></p>");
                            }
                        ?>
                    </center>
                </article>
            </section>
    
            <article class="shadow-lg bg-dark rounded p-5 d-flex flex-column justify-content-around m-5">
                <table class="colored mt-3 mb-3">
                    <thead>
                        <tr>
                            <th > ID </th>
                            <th > Nom </th>
                            <th > Prénom </th>
                            <th > Mail </th>
                            <th > Description </th>
                            <th > Priorité </th>
                            <th > Secteur </th>
                            <th > Statut </th>
                            <th > Date </th>
                        </tr>  
                    </thead>
                    <tbody>
                        <?php
                            selectAllAndDisplay($pdo);
                        ?>    
                    </tbody>
                </table>
            </article>
            <footer class="pied-de-page bg-dark mt-5 d-flex flex-column">
                <p class="colored text-center">
                    Copyright &copy; 2022 BirdZoo. All right reserved.
                </p>
            </footer>
        </body>
    </html>
</code>