<?php

session_start();

$login="";
$pass="";
$msg="";

if(isset($_SESSION["name"])){
    $login=$_SESSION["name"];
}

if(isset($_SESSION["password"])){
    $pass=$_SESSION["password"];
}

if(isset($_GET["msg"])){
    $msg=$_GET["msg"];
}

?>

<code><!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>BirdZoo</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
            <!-- Bootstrap -->
            <!-- jQuery library -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <!-- Popper JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <!-- Latest compiled JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        
            <!-- Font Awesome -->
            <script src="https://kit.fontawesome.com/541db90648.js" crossorigin="anonymous"></script>
        
            <link rel="icon" type="image/png" href="R.jfif">

            <link rel="stylesheet" href="style.css">
            

        </head>

      
        <body class="bg-dark">

            <section>
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top p-0 pl-1">
                <a class="navbar-brand" > 

                </a>
            
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item" id="navigation-accueil">
                            <a class="nav-link" href="index.php">Homepage</a>
                        </li>
                        <li class="nav-item" id="navigation-evenement">
                            <a class="nav-link" href="discover.php">Discover</a>
                        </li>
                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="map.php">Préparer sa visite</a>
                        </li>
                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="formTicket.php">Réclamation</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <?php 
                                if($login!=""){
                                    echo("<li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"profile.php\">$login</a> </li>
                                        <li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"./disconnect.php\">Déconnexion</a> </li>");
                                }
                                else{
                                    echo("<a class=\"nav-link\" href=\"connexion.php\">Connexion</a>");
                                }
                            ?>
                        </li>
            
                        
                    </ul>
                </div>
            </section>

    
            <section class="element mt-5">
                <article class="shadow-lg bg-dark rounded p-5">
                    <center>
                        <h2 class="mb-3"><i>Compte</i></h2>
                        <text class=" colored"> User currently connected :</text>
                        <?php
                            echo("<text class=\" colored\"><i>$login</i></text>");
                        ?>
                        <p class="p-3">
                            <a href="connexion.php"  class="btn btn-secondary">Changer d'utilisateur</a>
                            <a href="disconnect.php"  class="btn btn-secondary mt-3">Déconnexion</a>
                        </p>
                    </center>
                </article>

                <article class="shadow-lg bg-dark rounded p-5">
                    <center>
                        <i><h2> Flux <text class="colored">users</text> </h2></i>
                        <table class="colored mt-3">
                            <thead>
                                <tr>
                                    <th > ID </th>
                                    <th > USER </th>
                                </tr>  
                            </thead>
                            <tbody>
                                <?php
                                    if($login!="admin"){

                                        echo("<text class=\"mt-3 colored\">$login</text> n'a pas tout les privilèges <br>");
                                        echo("<tr>");
                                        echo("<th> 0 </th>"."<th> None </th>");
                                        echo("</tr>");
                                    }

                                    else{
                                        $servername = "mysql-birdzoo.alwaysdata.net";
                                        $dbname = "birdzoo_bdd";
                                        $username = "birdzoo";
                                        $mdp ="6z9r2uYT";

                                        try{
                                            $pdo = new PDO("mysql:host=$servername;dbname=$dbname", "$username", "$mdp");
                                        }

                                        catch(PDOException $pdoe){
                                            echo("Cannot access to the database !!!");
                                            exit();
                                        }
                                        
                                        // Select ALL USERS FROM DB IF ADMIN IS CONNECTED
                                        
                                        $sql_query = "SELECT * FROM `user`;";
                                        $statement = $pdo->query($sql_query);
                                        
                                        if( $statement === FALSE ){
                                            echo("Bad request ($sql_query)");
                                            exit();
                                        }
                                            
                                        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
                                        if($results){
                                            foreach($results as $entry) {
                                                echo("<tr>");
                                                echo("<th>".$entry['id']."</th>"."<th>".$entry['login']."</th>");
                                                echo("</tr>");
                                            } 
                                        }
                                                        
                                    }
                                ?>
                            </tbody>
                        </table>
                    </center>
                </article>
            </section>

            <section class="element mt-5">
                <article class="shadow-lg bg-dark rounded p-5">
                    <center>
                        <h2><i>Visites</i></h2><br>
                        <text class="colored">Work in progress... </text>
                    </center>
                </article>

                <article class="shadow-lg bg-dark rounded p-5">
                    <center>
                        <i><h2>Activités de <?php echo("<text class=\"colored\">$login</text>");?></h2></i>
                        <?php 
                                if($login=="admin"){
                                    echo("<a class=\"mt-3 btn btn-secondary\" href=\"afficherTicket.php\">Consulter les tickets</a>");
                                    echo("<a class=\"mt-3 btn btn-secondary\" href=\"formTicket.php\">Enregistrer un ticket</a>");
                                    
                                }
                                else{
                                    echo("<a class=\"mt-3 btn btn-secondary\" href=\"formTicket.php\">Enregistrer un ticket</a>");
                                }
                        ?>   
                    </center>
                </article>
            </section>
        
        </body>

    
        <footer class="pied-de-page bg-dark mt-5 d-flex flex-column">
            <p class="colored text-center">
                Copyright &copy; 2022 BirdZoo. All right reserved.
            </p>
        </footer>

    </html>
</code>

