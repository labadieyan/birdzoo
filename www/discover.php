<?php

session_start();
$login="";
if(isset($_SESSION["name"])){
    $login=$_SESSION["name"];
}

$heure = date("H+2:i");

?>

<code><!DOCTYPE html>
    <html>
    
        <head>
            <meta charset="utf-8">
            <title>BirdZoo</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
            <!-- Bootstrap -->

            <!-- jQuery library -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <!-- Popper JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <!-- Latest compiled JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <!-- CSS Button -->
            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        
            <!-- Font Awesome -->
            <script src="https://kit.fontawesome.com/541db90648.js" crossorigin="anonymous"></script>
        
            <!-- Logo Ouverture Site-->
            <link rel="icon" type="./images/logo.png">

            <!-- CSS Style -->

            <link rel="stylesheet" href="style.css">

        </head>


        <body class="bg-dark">
            <nav class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top p-0 pl-1">
                <a class="navbar-brand" > 

                </a>
            
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav mx-auto">

                        <li class="nav-item" id="navigation-accueil">
                            <a class="nav-link" href="index.php">Homepage</a>
                        </li>

                        <li class="nav-item" id="navigation-evenement">
                            <a class="nav-link" href="discover.php">Discover</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="map.php">Préparer sa visite</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="formTicket.php">Réclamation</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <?php 
                                if($login!=""){
                                    echo("<li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"profile.php\">$login</a> </li>
                                        <li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"./disconnect.php\">Déconnexion</a> </li>");
                                }
                                else{
                                    echo("<a class=\"nav-link\" href=\"connexion.php\">Connexion</a>");
                                }
                            ?>
                        </li>

                    </ul>
                </div>
                
                
                <article class="shadow-lg bg-dark rounded p-5 d-flex flex-column justify-content-around m-5">
                    <h2 class="mt-2" style="text-align:center"><strong>Outdoor Exhibits</strong></h2>
                    <table class="colored mt-3 mb-3">
                        <tr>
                            <th><header style="text-align:center">African Animals</header></th>
                            <th><header style="text-align:center">Tropical Animals</th>
                            <th><header style="text-align:center">Parrot</th>
                            <th><header style="text-align:center">European Animals</th>
                        </tr>
                        <tr>
                            
                        <th><div class="m-3"><center><img src="./images/1.jpg" style="height:200px; width:200px;"></center></div></th>
                        <th><div class="m-3"><center><img src="./images/2.jfif" style="height:200px; width:200px;"></center></div></th>
                        <th><div class="m-3"><center><img src="./images/3.jfif" style="height:200px; width:200px;"></center></div></th>
                        <th><div class="m-3"><center><img src="./images/4.jpg" style="height:200px; width:200px;"></center></div></th>
                            
                        </tr>
                        <tr>
                        <th>
                            Description: African birds are able to migrate to Europe when weather is too warm 
                        </th>
                        <th>
                            Desription: Tropicals birds are maybe one of the strongest animals, they can survive in the big cold
                        </th>
                        <th>
                            Description: The parrots are the ones to conquer the sky
                        </th>
                        <th>
                            Description: They are uncommon, for some we can't imagine how they even exists ? For others they usually live far from our continent.<br>You will be impressed 
                        </th>
                        </tr>
                        <tr>
                        <th>Surface area : 5500 m²</th>
                        <th>Surface area : 4500m²</th>
                        <th>Volume area : 5000m³</th>
                        <th>Surface area : 7000m²</th>
                        </tr>
                        <tr>
                        <th>
                            Number of animals:15
                        </th>
                        <th>
                            Number of animals : 25
                        </th>
                        <th>
                            Number of animals : 50
                        </th>
                        <th>
                            Number of animals : 17
                        </th>
                        </tr>
                        <tr>
                        <th>
                            Maximum capacity : 30
                        </th>
                        <th>
                            Maximum capacity : 80
                        </th>
                        <th>
                            Maximum capacity : 30
                        </th>
                        <th>
                            Maximum capacity : 20
                        </th>
                        </tr>
                        </table>
                    </article>
            
            <section class="element mt-5">
                <article class="shadow-lg bg-dark rounded p-5">
                    <center>
                        <h2><strong>Planning</strong></h2><br>
                        <text class="colored">Work in progress... </text>
                    </center>
                </article>

                <article class="shadow-lg bg-dark rounded p-5">
                    <center>
                       <h2><strong>Ticket Reservation</strong></h2><br>
                       <text class="colored">Work in progress... </text>
                    </center>
                </article>
            </section>

            <footer class="pied-de-page bg-dark mt-5 d-flex flex-column">
                <p class="colored text-center">
                    Copyright &copy; 2022 BirdZoo. All right reserved.
                </p>
            </footer>                    
        </body>
    </html>
</code>