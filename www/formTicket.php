<?php

    session_start();
    $login="";
    if(isset($_SESSION["name"])){
        $login=$_SESSION["name"];
    }

    $msg="";
    if(isset($_GET["msg"])){
        $msg=$_GET["msg"];
    }
    
    $msg2="";
    if(isset($_GET["msg2"])){
        $msg2=$_GET["msg2"];
    }
?>

<code><!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <title>BirdZoo</title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
      <!-- Bootstrap -->
      <!-- jQuery library -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <!-- Popper JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  
      <!-- Font Awesome -->
      <script src="https://kit.fontawesome.com/541db90648.js" crossorigin="anonymous"></script>
  
      <link rel="icon" type="image/png" href="R.jfif">

      <link rel="stylesheet" href="style.css">


    </head>

    <body class="bg-dark">

    <section>
        <nav class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top p-0 pl-1">
        <a class="navbar-brand" > 

        </a>
    
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item" id="navigation-accueil">
                    <a class="nav-link" href="index.php">Homepage</a>
                </li>
                <li class="nav-item" id="navigation-evenement">
                    <a class="nav-link" href="discover.php">Discover</a>
                </li>
                <li class="nav-item" id="navigation-stream">
                    <a class="nav-link" href="map.php">Préparer sa visite</a>
                </li>
                <li class="nav-item" id="navigation-stream">
                    <a class="nav-link" href="formTicket.php">Réclamation</a>
                </li>

                <li class="nav-item" id="navigation-stream">
                    <?php 
                        if($login!=""){
                            echo("<li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"profile.php\">$login</a> </li>
                                  <li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"./disconnect.php\">Déconnexion</a> </li>");
                        }
                        else{
                            echo("<a class=\"nav-link\" href=\"connexion.php\">Connexion</a>");
                        }
                    ?>
                </li>
    
                
            </ul>
        </div>
    </section>


       
        
    <section class="pad ">
        <article class=" shadow-lg bg-dark rounded p-5">

            <?php 
                echo("<h4 class=\"colored\">$msg</h4>");
            ?>

            <form class="form bg-dark" method="post" action="./recupTicket.php">
                <center><h2>CONTACT</h2></center>
                <section class="radius element mt-5">
                    <article class="shadow-lg bg-dark rounded p-5">
                        <center><h5 class="mb-3"><i>Coordonnées</i></h5></center>
                        <p><input class="col-sm-12 form-control mt-3" name="name" placeholder="Prénom"></input></p>
                        <p><input class="col-sm-12 form-control mt-3" name="surname" placeholder="Nom"></input></p>
                        <p><input class="col-sm-12 form-control mt-3" name="mail" placeholder="E-mail"></input></p>
                        <p><input class="col-sm-12 form-control mt-3" maxlength="600" name="dsc" placeholder="Description"></input></p>
                    </article>
                    
                    <article class="shadow-lg bg-dark rounded p-5">
                        <center><h5><i>Priorité et lieu de l'incident</i></h5></center>
                        <p class="mt-4 mr-2 colored"><input class="checkbox" type="radio" name="prio" value="High"> Haut   </input></p>
                        <p class="mt-4 mr-2 colored"><input class="checkbox" type="radio" name="prio" value="Medium"> Moyen </input></p>
                        <p class="mt-4 mr-2 colored"><input class="checkbox" type="radio" name="prio" value="Low"> Faible </input></p>
                        <?php echo("<text class=\"error\">$msg2</text>");?>
                        <p class="mt-5"><input class="col-sm-12 form-control" name="location" placeholder="Lieu de l'incident"></input></p>
                    </article>
                </section>
                
                <section class="element mt-5">
                    <article class="shadow-lg bg-dark rounded p-5 mb-5">
                        <center><h5 class="mb-3"><i>Date de l'incident</i></h5></center>
                        <p><input class="col-sm-12 form-control mt-3" type="date" name="date"></p>
                        <input type="time" class="col-sm-12 form-control" ></input>
                    </article>

                    <article class="shadow-lg bg-dark rounded p-5 mb-5">
                        <center><h5 class="mb-3"><i>Statut de l'incident</i></h5></center>
                        <p class="mt-2 mr-2 colored"><input  type="radio" name="statut" value="En cours"> En cours </input></p>
                        <?php echo("<text class=\"error\">$msg2</text>");?>
                        <br><br>
                        <text class="colored mt-3"> This is the </text><strong>only</strong><text class="colored"> one option </text>
                    </article>
                    
                </section>
                
                <center><input class="btn btn-secondary" type="submit" value="Envoyer" /></center>
                <div class="mt-5">
                    <text class="colored"> If you have any problem with </text><strong>Ticket registering</strong><text class="colored">, please contact us : </text>
                    <span class="fa fa-phone p-1 pt-4"> 001 220 332 255</span>
                    <span class="fa fa-envelope-o p-1 pt-4"> contact@company.com</span> 
                </div>
                
            </form>
        </article>
  
    </section>

    
    <footer class="pied-de-page bg-dark mt-auto d-flex flex-column">
            <p class="fin  text-center">
                Copyright &copy; 2022 BirdZoo. Tous droits réservés.
            </p>
    </footer>

</body>