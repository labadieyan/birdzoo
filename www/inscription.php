<?php 
    
    $msg="";
    if(isset($_GET["msg"])){
        $msg=$_GET["msg"];
    }

    $msg2="";
    if(isset($_GET["msg2"])){
        $msg=$_GET["msg2"];
    }

    session_start();
    $login="";
    if(isset($_SESSION["name"])){
        $login=$_SESSION["name"];
    }

?>

<code><!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>BirdZoo</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
            <!-- Bootstrap -->
            <!-- jQuery library -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <!-- Popper JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <!-- Latest compiled JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        
            <!-- Font Awesome -->
            <script src="https://kit.fontawesome.com/541db90648.js" crossorigin="anonymous"></script>
        
            <link rel="icon" type="image/png" href="R.jfif">

            <link rel="stylesheet" href="style.css">
            <link rel="stylesheet" href="form.css" >


        </head>

        <body class="bg-dark">

            <section>
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top p-0 pl-1">
                <a class="navbar-brand" > 

                </a>
            
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item" id="navigation-accueil">
                            <a class="nav-link" href="index.php">Homepage</a>
                        </li>
                        <li class="nav-item" id="navigation-evenement">
                            <a class="nav-link" href="zoo.html">Découvrir le Zoo</a>
                        </li>
                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="visite.html">Préparer sa visite</a>
                        </li>
                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="formTicket.php">Réclamation</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <?php 
                                if($login!=""){
                                    echo("<li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"profile.php\">$login</a> </li>
                                        <li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"./disconnect.php\">Déconnexion</a> </li>");
                                }
                                else{
                                    echo("<a class=\"nav-link\" href=\"connexion.php\">Connexion</a>");
                                }
                            ?>
                        </li>
                            
                    </ul>
                </div>
            </section>
        
            <section class="element mt-5">
                <!-- INSCRIPTION FORM
             -->
                <article class="shadow-lg bg-dark rounded p-5">
                    <form class="form" action="./inscrip.php" method="post">
                        <center>
                            <h2><i>Inscription</i></h2>
                            <p> Veuillez entrer vos informations personnelles afin de vous inscrire : </p>
                            <p><input name="login" class="col-sm-12 form-control" type="text" placeholder="Nom d'utilisateur"></input></p>
                            <p><input name="pass" class="col-sm-12 form-control" type="password" placeholder="Mot de passe"></input></p>
                            <p><input name="cpass" class="col-sm-12 form-control" type="password" placeholder="Confirmez mot de passe"></input></p>
                            <p><input name="date" class="col-sm-12 form-control" type="date"></p>
                            <p class="error"><?php echo("<p class=\"error\">$msg</p>");?></p>
                            <p class="error"><?php echo("<p class=\"error\">$msg2</p>");?></p>
                            <input class="btn btn-secondary mt-2" type="submit" value="Valider"/>
                        </center>          
                    </form>
                    <center>
                        <text class="colored"> If you have any problem with </text>
                        <strong>Inscription</strong>
                        <text class="colored">  please register a </text>
                        <strong><a class="colored" href="./formTicket.php">Ticket</a></strong>
                    </center>
                </article>
            </section>

            <footer class="pied-de-page bg-dark mt-5 d-flex flex-column">
                <p class="colored text-center">
                    Copyright &copy; 2022 BirdZoo. All right reserved.
                </p>
            </footer>

        </body>
    </html>
</code>
