<?php

session_start();
$login="";
if(isset($_SESSION["name"])){
    $login=$_SESSION["name"];
}

$heure = date("H+2:i");

?>

<code><!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>BirdZoo</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
            <!-- Bootstrap -->

            <!-- jQuery library -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <!-- Popper JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <!-- Latest compiled JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <!-- CSS Button -->
            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        
            <!-- Font Awesome -->
            <script src="https://kit.fontawesome.com/541db90648.js" crossorigin="anonymous"></script>
        
            <!-- Logo Ouverture Site-->
            <link rel="icon" type="./images/logo.png">

            <!-- CSS Style -->

            <link rel="stylesheet" href="style.css">

        </head>


        <body class="bg-dark">
            <nav class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top p-0 pl-1">
                <a class="navbar-brand" > 

                </a>
            
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav mx-auto">

                        <li class="nav-item" id="navigation-accueil">
                            <a class="nav-link" href="index.php">Homepage</a>
                        </li>

                        <li class="nav-item" id="navigation-evenement">
                            <a class="nav-link" href="discover.php">Discover</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="map.php">Préparer sa visite</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="formTicket.php">Réclamation</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <?php 
                                if($login!=""){
                                    echo("<li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"profile.php\">$login</a> </li>
                                        <li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"./disconnect.php\">Déconnexion</a> </li>");
                                }
                                else{
                                    echo("<a class=\"nav-link\" href=\"connexion.php\">Connexion</a>");
                                }
                            ?>
                        </li>

                    </ul>
                </div>
                
                <section class="element mt-5">
                    <article class="shadow-lg bg-dark rounded p-5">
                        <center>
                            <h3>Horaires d'ouvertures</h3><br> 
                            <p class="colored align-self-center">
                                
                                Lundi : 8h - 18h <br>
                                Mardi : 8h - 18h <br>
                                Mercredi : 8h - 18h <br>
                                Jeudi : 8h - 18h <br>
                                Vendredi : 8h - 18h <br>
                                Samedi : 8h - 18h <br><br>

                                Exceptional opening on July 14th ! 
                            </p>

                        
                            <p class="p-3"> Current time : </p>
                            <div>
                                <script>
                                date = new Date().toLocaleDateString();
                                document.write(date);
                                </script>
                            </div>
                            
                            <?php 
                                echo strftime('%H+:%M UTC+0');
                                //header('refresh: 30');
                            ?>
                        </center>
                        
                    </article>  

                    <article class="shadow-lg bg-dark rounded p-5">
                        <center>
                            <h4 class=" align-self-center"><strong>Location</strong></h4>
                            <img src="./images/maps.png" class="mt-4" style="height:200px; width:300px;">
                            <p class="mt-4"><i class="colored">Sortie 17b A15</i><a><strong><i> Conflans fin d'Oise</i></a></strong></p>
                        </center>
                    </article>
                </section>
                
                <section class="element mt-5">
                    
                    <article class=" shadow-lg bg-dark rounded p-5 d-flex flex-column justify-content-around">
                        <h3 class="align-self-center">Visite</h3>
                        <p class="colored align-self-center">
                            En étant <strong>membre</strong> vous accederez a des <strong>avantages</strong> 
                            divers comme des <strong>réductions</strong> 
                            exclusives, et bien d'autres encore ! Rendez-vous dans l'onglet « Visite » de votre 
                            espace <strong> client</strong>
                        </p> 

                        <div class="align-self-center">
                            <a href="connexion.php"  class="btn btn-secondary">Connexion</a>
                            <a href="inscription.php"  class="btn btn-secondary">Inscription</a>
                        </div>
                    
                    </article>

                    <article class="shadow-lg bg-dark rounded p-5">
                        <center>
                            <h4 class=" align-self-center"><strong>Espace Familiale</strong></h4>
                            <img src="./images/map.jpg" class="mt-4" style="height:250px; width:300px;">
                            <p class="mt-4"><i class="colored">En savoir </i><a><strong><i>plus !</i></a></strong></p>
                        </center>
                    </article>

                </section>

                <!-- Footer = pied de page -->

                <footer class="pied-de-page bg-dark mt-auto d-flex flex-column">
                    <section class="d-flex flex-row justify-content-around pt-3 ">
                        
                        <ul class="text-center">
                            <li><a>Politique de confidentialité</a></li>
                            <li><a>Conditions Générales d'utilisation</a></li>
                            <li><a>Informations Légales</a></li>
                            <li><a>F.A.Q</a></li>
                        </ul>
                                
                        <ul>
                            <li>
                                <a><i class="fa fa-instagram" aria-hidden="true"></i> Suivez nous sur Instagram </a>
                            </li>
                            <li>
                                <a><i class="fa fa-twitter" aria-hidden="true"></i> Suivez nous sur Twitter </a>
                            </li>
                        </ul>
                    </section>
                
                    <p class="colored text-center">
                        Copyright &copy; 2022 BirdZoo. All right reserved. Powered by L.Yannis
                    </p>
                </footer>
        </body>
    </html>
</code>