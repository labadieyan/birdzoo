<?php

// SESSION START FOR TESTING WHO CONNECTED

session_start();
$login="";
if(isset($_SESSION["name"])){
    $login=$_SESSION["name"];
}

// MISSION : HOUR DISPLAY 

$heure = date("H+2:i");

?>

<code><!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <title>BirdZoo</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
            <!-- Bootstrap -->

            <!-- jQuery library -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <!-- Popper JS -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
            <!-- Latest compiled JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <!-- CSS Button -->
            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        
            <!-- Font Awesome -->
            <script src="https://kit.fontawesome.com/541db90648.js" crossorigin="anonymous"></script>
        
            <!-- Logo Ouverture Site-->
            <link rel="icon" type="image/png" sizes="16x16" src="./favicon.ico">
            
            <!-- CSS Style -->

            <link rel="stylesheet" href="style.css">

        </head>


        <body class="bg-dark">
            <nav class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top p-0 pl-1">
                <a class="navbar-brand" > 

                </a>
            
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- NAVIGATION BAR -->

                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav mx-auto">

                        <li class="nav-item" id="navigation-accueil">
                            <a class="nav-link" href="index.php">Homepage</a>
                        </li>

                        <li class="nav-item" id="navigation-evenement">
                            <a class="nav-link" href="discover.php">Discover</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="map.php">Préparer sa visite</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <a class="nav-link" href="formTicket.php">Réclamation</a>
                        </li>

                        <li class="nav-item" id="navigation-stream">
                            <?php 
                                if($login!=""){
                                    echo("<li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"profile.php\">$login</a> </li>
                                        <li class=\"nav-item\" id=\"navigation-stream\"> <a class=\"nav-link\" href=\"./disconnect.php\">Déconnexion</a> </li>");
                                }
                                else{
                                    echo("<a class=\"nav-link\" href=\"connexion.php\">Connexion</a>");
                                }
                            ?>
                        </li>

                    </ul>
                </div>

                <!-- 4 SUBCATEGORIES OF WEBSITE ( MISSION ) : 2 SECTION CONTAIN 2 ARTICLE -->
                
                <section class="element mt-5">
                    <article class="shadow-lg bg-dark rounded p-5">
                        <center>
                            <h3 ><i> Horaires d'ouvertures </i></h3><br> 
                            <p class="colored align-self-center">
                                
                                Lundi : 8h - 18h <br>
                                Mardi : 8h - 18h <br>
                                Mercredi : 8h - 18h <br>
                                Jeudi : 8h - 18h <br>
                                Vendredi : 8h - 18h <br>
                                Samedi : 8h - 18h <br><br>

                                Exceptional opening on July 14th ! 
                            </p>

                             <!-- HOUR / DATE DISPLAY  -->
                            <p class="p-3"> Current time : </p>
                            <div>
                                <script>
                                date = new Date().toLocaleDateString();
                                document.write(date);
                                </script>
                            </div>
                            
                            <?php 
                                echo strftime('%H+:%M UTC+0');
                                //header('refresh: 30'); UPDATING TIME EVERY 30 sec BY REFRESHING PAGE
                            ?>
                        </center>
                        
                    </article>  
                
                
                    <article class=" shadow-lg bg-dark rounded p-5 d-flex flex-column justify-content-around">
                        <h3 class="align-self-center"><i>Déjà visiteur ?</i></h3>
                        <p class="colored align-self-center">
                            En étant <strong>membre</strong> vous accederez a des <strong>avantages</strong> 
                            divers comme des <strong>réductions</strong> 
                            exclusives, et bien d'autres encore ! Rejoignez nous !
                        </p> 

                        <div class="align-self-center">
                            <a href="connexion.php"  class="btn btn-secondary">Connexion</a>
                            <a href="inscription.php"  class="btn btn-secondary">Inscription</a>
                        </div>
                    
                    </article> 

                </section>
                
                
                <center>
                    <section id="carouselAccueil" class="carousel slide align-self-center mt-5" data-ride="carousel">
                                
                                    <ul class="carousel-indicators">
                                        <li data-target="#carouselAccueil" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselAccueil" data-slide-to="1"></li>
                                        <li data-target="#carouselAccueil" data-slide-to="2"></li>
                                    </ul>
                            
                                    <div class="carousel-inner">
                                        <div class="carousel-item active"  id="carousel1">
                                            <a href="./inscription.php"> 
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <video class="embed-responsive-item" muted>
                                                        <source 
                                                            src="./images/1.mp4" 
                                                            type="video/mp4">

                                                        Désolé, votre navigateur ne supporte pas la vidéo
                                                    </video>
                                                </div>
                                            </a>
                                        </div>
                            
                                        <div class="carousel-item "  id="carousel1">
                                            <a href="./inscription.php"> 
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <video class="embed-responsive-item" muted>
                                                        <source 
                                                            src="./images/1.mp4" 
                                                            type="video/mp4">

                                                        Désolé, votre navigateur ne supporte pas la vidéo
                                                    </video>
                                                </div>
                                            </a>
                                        </div>
                            
                                        <div class="carousel-item "  id="carousel1">
                                            <a href="./inscription.php"> 
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <video class="embed-responsive-item" muted>
                                                        <source 
                                                            src="./images/1.mp4" 
                                                            type="video/mp4">

                                                        Désolé, votre navigateur ne supporte pas la vidéo
                                                    </video>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                            
                            
                                    <a class="carousel-control-prev" href="#carouselAccueil" data-slide="prev">
                                        <span class="carousel-control-prev-icon"></span>
                                        <span class="sr-only">Précédent</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselAccueil" data-slide="next">
                                        <span class="carousel-control-next-icon"></span>
                                        <span class="sr-only">Suivant</span>
                                    </a>
                    </section>
                </center>

                <section class="element mt-5">
                    
                    <article class="shadow-lg bg-dark rounded p-5">
                        <center>
                            <h4 class=" align-self-center"><strong>Free Space</strong></h4>
                            <img src="./images/2.jfif" class="mt-4" style="height:200px; width:300px;">
                            <p class="mt-4"><i class="colored">En savoir </i><a><strong><i>plus !</i></a></strong></p>
                        </center>
                    </article>
                
                    <article class="shadow-lg bg-dark rounded p-5">
                        <h4 class=" align-self-center"><center><strong>Join us on Youtube</strong></center></h4> 
                        <p class=" align-self-center">
                            <main class="d-flex flex-column text-justify mt-5">    
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe
                                        class="embed-responsive-item" 
                                        src="https://www.youtube.com/embed/jRcmrwNodYM" 
                                        allowfullscreen>
                                    </iframe>   
                                </div>
                            </main>
                        </p>
                        <center><p class="mt-4"><i class="colored">Source : </i><a><strong><i>BioParc Inc.</i></a></strong></p></center>
                    </article>

                </section>

                <!-- Footer = pied de page -->

                <footer class="pied-de-page bg-dark mt-auto d-flex flex-column">
                    <section class="d-flex flex-row justify-content-around pt-3 ">
                        
                        <ul class="text-center">
                            <li><a>Politique de confidentialité</a></li>
                            <li><a>Conditions Générales d'utilisation</a></li>
                            <li><a>Informations Légales</a></li>
                            <li><a>F.A.Q</a></li>
                        </ul>
                                
                        <ul>
                            <li>
                                <a><i class="fa fa-instagram" aria-hidden="true"></i> Suivez nous sur Instagram </a>
                            </li>
                            <li>
                                <a><i class="fa fa-twitter" aria-hidden="true"></i> Suivez nous sur Twitter </a>
                            </li>
                        </ul>
                    </section>
                
                    <p class="colored text-center">
                        Copyright &copy; 2022 BirdZoo. All right reserved. Powered by L.Yannis
                    </p>
                </footer>
        </body>
    </html>
</code>