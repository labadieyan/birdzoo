-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : dim. 29 mai 2022 à 21:49
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `report`
--

-- --------------------------------------------------------

--
-- Structure de la table `reports`
--


CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `surname` text NOT NULL,
  `mail` text NOT NULL,
  `dsc` text NOT NULL,
  `prio` text,
  `location` text NOT NULL,
  `time` date DEFAULT NULL,
  `statut` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reports`
--

INSERT INTO `reports` (`id`, `name`, `surname`, `mail`, `dsc`, `prio`, `location`, `time`, `statut`) VALUES
(10, 'Lola', 'Iyaa', 'loliyaa@gmail.com', 'Remplacement poteau', 'Low', 'EntrÃ©e parc', '2022-07-22', 'Solved'),
(9, 'Yani', 'Ago', 'yanagoagmail', 'Fuite d eau', 'High', 'Toilettes', '2022-07-22', 'Solved'),
(14, 'Yanis', 'Lad', 'labadyan@gmail.com', 'Fuite', 'Medium', 'Toilette', '2022-05-18', 'Solved'),
(13, 'Lokman', 'Nokman', 'lokmnarobase', 'DS3 en panne', 'Low', 'Parking', '2018-07-22', 'Solved'),
(19, 'Test', 'Test', 'Test', 'Test', 'Medium', 'Test', '0001-11-03', 'En cours');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
